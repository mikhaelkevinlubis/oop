<?php
require ('animal.php');
require ('frog.php');
require ('ape.php');

$sheep = new Animal("shaun");

echo "Nama hewan: " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah kaki: " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded? " . $sheep->cold_blooded . "<br> <br>"; // "no"

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk"); 
$kodok->jump() ; // "hop hop"

?>